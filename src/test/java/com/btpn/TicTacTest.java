package com.btpn;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Unit test for tic toe App.
 */
public class TicTacTest {
    @Test
    public void shouldReturnTrue_whenPlayerXOccupiesAllHorizontalLine() {
        char input[] = { 'X', 'X', 'X', 'O', 'O', 'X', 'X', 'O', 'X' };
        TicTac ticTac = new TicTac(input);
        assertEquals(true, ticTac.isPlayerWin('X'));
    }

    @Test
    public void shouldReturnTrue_whenPlayerOOccupiesAllHorizontalLine() {
        char input[] = { 'O', 'O', 'O', 'X', 'X', 'O', 'O', 'X', 'O' };
        TicTac ticTac = new TicTac(input);
        assertEquals(true, ticTac.isPlayerWin('O'));
    }

    @Test
    public void shouldReturnTrue_whenPlayerXOccupiesAllVerticalLine() {
        char input[] = { 'X', '0', 'X', 'X', 'O', 'X', 'X', 'O', 'X' };
        TicTac ticTac = new TicTac(input);
        assertEquals(true, ticTac.isPlayerWin('X'));
    }

    @Test
    public void shouldReturnTrue_whenPlayerOOccupiesAllVerticalLine() {
        char input[] = { 'O', 'X', 'O', 'O', 'X', 'O', 'O', 'X', 'O' };
        TicTac ticTac = new TicTac(input);
        assertEquals(true, ticTac.isPlayerWin('O'));
    }

    @Test
    public void shouldReturnTrue_whenPlayerXOccupiesAllDiagonalLine() {
        char input[] = { 'O', 'O', 'X', 'O', 'X', 'O', 'X', 'X', 'O' };
        TicTac ticTac = new TicTac(input);
        assertEquals(true, ticTac.isPlayerWin('X'));
    }

    @Test
    public void shouldReturnTrue_whenPlayerOOccupiesAllDiagonalLine() {
        char input[] = { 'O', 'O', 'X', 'X', 'O', 'O', 'X', 'X', 'O' };
        TicTac ticTac = new TicTac(input);
        assertEquals(true, ticTac.isPlayerWin('O'));
    }

    @Test
    public void shouldReturnTrue_whenThereAreNoPlayerOccupiesAllVerticalHorizontalOrDiagonalLines() {
        char input[] = { 'O', 'X', 'O', 'X', 'O', 'X', 'X', 'O', 'X' };
        TicTac ticTac = new TicTac(input);
        assertEquals(true, ticTac.isDraw());
    }

    @Test
    public void shouldPrintGameStillInProgress_whenTheGameIsInProgress() {
        char input[] = { 'O', 'X', 'O', 'X', '-', '-', 'X', '-', '-' };
        TicTac ticTac = new TicTac(input);
        assertEquals("Game still in progress!", ticTac.printCurrentState());
    }

    // xCount -> total X in game board
    // oCount -> total O in game board
    @Test
    public void shouldPrintInvalidGameBoard_whenXCountIsNotEqualOCount() {
        char input[] = { 'X', 'X', 'X', 'O', 'O', 'O', 'X', 'O', 'O' };
        TicTac ticTac = new TicTac(input);
        assertEquals("Invalid game board", ticTac.printCurrentState());
    }

    @Test
    public void shouldPrintInvalidGameBoard_whenXCountIsNotEqualOCountPlus1() {
        char input[] = { 'X', 'X', 'O', 'X', 'O', 'X', 'X', 'O', 'X' };
        TicTac ticTac = new TicTac(input);
        assertEquals("Invalid game board", ticTac.printCurrentState());
    }

    @Test
    public void shouldPrintInvalidGameBoard_whenMoreThanOnePlayerWins() {
        char input[] = { 'X', 'X', 'X', 'O', 'O', 'O', 'X', 'X', 'O' };
        TicTac ticTac = new TicTac(input);
        assertEquals("Invalid game board", ticTac.printCurrentState());
    }

    @Test
    public void shoudlPrintInvalidGameBoard_whenXWinsAndXCountIsNotEqualOCountPlus1() {
        char input[] = { 'X', 'X', 'X', 'O', 'X', 'O', 'X', 'X', 'O' };
        TicTac ticTac = new TicTac(input);
        assertEquals("Invalid game board", ticTac.printCurrentState());
    }
}
