package com.btpn;

import java.util.Scanner;

/**
 * Entry point
 *
 */
public class App {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (input.equals("")) {
            System.out.print("Game board: ");
            input = scanner.next();
        }
        scanner.close();
        TicTac ticTac = new TicTac(input.toCharArray());
        System.out.println(ticTac.printCurrentState());
    }
}
