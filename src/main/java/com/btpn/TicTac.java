package com.btpn;

import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TicTac {
    // 9 characters represent current board state in the game
    private char[] board;

    /*
     * board visualization 0 .. 8 index from the 9 characters input
     * | 0 | 1 | 2 |
     * | 3 | 4 | 5 |
     * | 6 | 7 | 8 |
     */
    // possible win state matrix
    private final int winState[][] = {
            // horizontal lines
            { 0, 1, 2 },
            { 3, 4, 5 },
            { 6, 7, 8 },
            // vertical lines
            { 0, 3, 6 },
            { 1, 4, 7 },
            { 2, 5, 8 },
            // diagonal lines
            { 0, 4, 8 },
            { 2, 4, 6 }
    };

    TicTac(char[] board) {
        this.board = board;
    }

    public boolean isPlayerWin(char player) {
        for (int i = 0; i < winState.length; i++) {
            if (this.board[winState[i][0]] == player
                    && this.board[winState[i][1]] == player
                    && this.board[winState[i][2]] == player) {
                return true;
            }
        }
        return false;
    }

    public String printCurrentState() {
        if (boardIsValid()) {
            if (isPlayerWin('X')) {
                return "X Wins!";
            } else if (isPlayerWin('O')) {
                return "O Wins!";
            } else if (isDraw()) {
                return "It's a draw!";
            } else {
                return "Game still in progress!";
            }
        } else {
            return "Invalid game board";
        }
    }

    public boolean isDraw() {
        List<Character> listC = new ArrayList<Character>();
        for (char c : board) {
            listC.add(c);
        }
        return !isPlayerWin('X') && !isPlayerWin('O') && !listC.stream().anyMatch(predicate -> predicate == '-');
    }

    public boolean boardIsValid() {
        // Assuming that player X plays first see :
        // https://en.wikipedia.org/wiki/Tic-tac-toe
        // Since player X plays first the valid board rules has to be :
        // 1. Total playing player X should equal with total playing player O or
        // 2. Total playing player X should equal with total playing player O plus 1
        // 3. Only allowed 1 player that wins the game
        // 4. Total playing player X should be greater than player O if X wins the game

        // count the number of X and O
        int xCount = 0, oCount = 0;
        for (char c : board) {
            if (c == 'X') {
                xCount++;
            }
            if (c == 'O') {
                oCount++;
            }
        }

        if (xCount == oCount || xCount == oCount + 1) {
            if (isPlayerWin('O') && isPlayerWin('X')) {
                return false;
            } else {
                if (isPlayerWin('X')) {
                    if (xCount != oCount + 1) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

}
